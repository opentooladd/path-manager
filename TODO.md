# TODO

- [x] Get list of .rc files (.bashrc, .profile, .zshrc, etc...)
    - [x] Select of file to modify ?
- [x] Ability to add a PATH item
    - [x] text
    - [x] path with file window
- [x] Delete a PATH
- [] Add search to path item list
- [] Add possibility to specify a custom file ? (for other shells)

- [] Add attributions section in app
    - [] Use Rust to read the file in the project and use it in the front
- [] Add versionning and package info
- [] Add about section
- [] Add Help

- [] Make Command line tool with same code ?

- [] Test Rust's functions
- [] E2E tests ? How ?
