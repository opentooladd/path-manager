# Path Manager application

An application to manage PATH env variable on a computer.
UNIX, Windows and MacOS.

## Functionalities

- Add path item with text or with a File Explorer
- Delete path items
- Choose source file in which to add the env variable (.bashrc, .zshrc, .profile)
- Save it ! (It automatically makes a backup of the file with .bak extension)

## DEV

### Dependencies

- [Rust](https://www.rust-lang.org/learn)
- [Nodejs](https://nodejs.org/en/)

Also see [Tauri](https://tauri.app/v1/guides/getting-started/setup/) dependencies for your specific platform.
see https://tauri.app/v1/guides/getting-started/prerequisites

### Workflow

This application only works on Desktop environment (maybe for now ?).
So the web build isn't useful yet.

To run the project use `make tauri-run`
it will build the project and open the program in an OS window.

To watch the code for change, use `make watch-tauri-dev`
**Watch is done** with `watchexec` executable installed via cargo globally.
There is a make command for it if you want `make install-watchexec`

### Build

To build the project for release use `make build-tauri-release`
Target will be in `src-tauri/targets/bundle`.
There should be an **AppImage**, an **executable** and a **.deb** package if you built on Linux.
Else on Windows there should be an **.msi** installer.
