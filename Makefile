SHELL := /bin/bash
UTILS_FOLDER = ~/Documents/NextCloud/BASH_UTILS/
SERVER_URI := http://localhost:5173

# build could fail if not run as sudo
# command: sudo -u ciro make build-tauri-release

all:
	make -j2 tauri-dev open-server

watch-tauri-dev:
	watchexec -r -w src make tauri-run

tauri-dev:
	npm run tauri dev

wait-for-server:
	@$(UTILS_FOLDER)wait_for_url.sh "$(SERVER_URI)"

open-server: wait-for-server
	xdg-open $(SERVER_URI)

tauri-run:
	npm run build
	# unset is necessary for dev run
	# else thread panick
	unset APPDIR && unset APPIMAGE && cd src-tauri && cargo run

build-tauri-dev:
	cd ./src-tauri && cargo build

build-tauri-debug: src-tauri/target/debug/app

build-tauri-release: src-tauri/target/release/app
	
update-packages:
	npm install @tauri-apps/cli@latest @tauri-apps/api@latest

src-tauri/target/debug/app: src-tauri/src/main.rs dist/index.html
	npm run tauri build --debug

src-tauri/target/release/app: node_modules src-tauri/src/main.rs dist/index.html
	npm run tauri build --release

dist/index.html:
	npm run build

node_modules:
	npm install

clean-frontend:
	rm -rf dist/

install-watchexec:
	cargo install watchexec-cli

clean-tauri:
	rm -rf ./src-tauri/target

clean: clean-frontend clean-tauri
	rm -rf dist
