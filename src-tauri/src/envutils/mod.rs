use std::{
    env,
    fs::{self, File, OpenOptions},
    io::{Read, Write},
    path::PathBuf,
};

const LIST_OF_POSSIBLE_SOURCE_FILES: [&str; 3] = [".bashrc", ".profile", ".zshrc"];

#[tauri::command]
pub fn get_list_of_source_files() -> Result<Vec<String>, String> {
    let home_dir: PathBuf = dirs::home_dir().expect("No HOME directory");
    let list_of_files: Vec<PathBuf> = LIST_OF_POSSIBLE_SOURCE_FILES
        .clone()
        .iter()
        .map(|filename| home_dir.join(filename))
        .filter(|path| path.try_exists().unwrap())
        .collect();
    Ok(list_of_files
        .iter()
        .map(|p| format!("{}", p.display()))
        .collect())
}

#[tauri::command]
pub fn write_source_file(filepath: PathBuf, path_vars: Vec<String>) -> Result<(), String> {
    let mut bakfilepath = filepath.clone();
    bakfilepath.set_extension("bak");
    fs::copy(&filepath, &bakfilepath).map_err(|e| format!("{} {}", bakfilepath.display(), e))?;
    let mut file = OpenOptions::new()
        .read(true)
        .open(&filepath)
        .map_err(|e| format!("{}", e))?;
    let mut content = String::new();
    file.read_to_string(&mut content)
        .map_err(|e| format!("{}", e))?;
    let path_env_string = format!(r#"PATH="{}""#, path_vars.join(":"));
    let new_content: String = if content.contains("PATH") {
        content
            .lines()
            .map(|line| {
                if line.contains("PATH") {
                    path_env_string.clone()
                } else {
                    line.to_owned() + "\n"
                }
            })
            .collect()
    } else {
        format!("{}\n{}", content, path_env_string)
    };
    fs::remove_file(&filepath).map_err(|e| format!("{}", e))?;
    let mut file = File::create(&filepath).map_err(|e| format!("{}", e))?;
    file.write(new_content.as_bytes())
        .map_err(|e| format!("{}", e))?;
    Ok(())
}

#[tauri::command]
pub fn get_path() -> Result<Vec<String>, String> {
    let mut vec: Vec<String> = vec![];
    match env::var_os("PATH") {
        Some(paths) => {
            for path in env::split_paths(&paths) {
                vec.push(format!("{}", path.display()))
            }
        }
        None => return Err(format!("There is not path in the environment")),
    };
    Ok(vec)
}
