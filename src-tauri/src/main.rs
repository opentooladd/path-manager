#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

pub mod envutils;

fn main() {
    match fix_path_env::fix() {
        Ok(()) => {
            tauri::Builder::default()
                .invoke_handler(tauri::generate_handler![
                    envutils::get_path,
                    envutils::get_list_of_source_files,
                    envutils::write_source_file
                ])
                .run(tauri::generate_context!())
                .expect("error while running tauri application");
        }
        Err(e) => println!("{}", e),
    }
}
